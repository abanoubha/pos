# POS App

Point-of-Sale software. POS App is created to be cross-platform and opensource.

This software is _under early development_.

## Goals & TODOs

- [x] [Gio](https://github.com/gioui/gio) UI design
- [ ] flatpak pkg & add to flathub
- [ ] add to Elementary AppCenter
- [ ] snap pkg & add to SnapStore
- [ ] homebrew for linux and Mac OS
- [ ] MacPorts for Mac OS
- [ ] add to microsoft store & winget
- [ ] downloadable exe (Windows) on website
- [ ] downloadable app (Mac) on website
- [ ] backup and restore locally
- [ ] backup and restore to remote server (Next Cloud, Google Drive, ..)

## Install POS App

- flatpak (flathub or AppCenter)
- snap (SnapStore)
- Homebrew (Linux and Mac OS)
- MacPorts (Mac OS)
- Windows (`winget` or store or downloadable.exe file)

## Want to help

- translate it to your native language (here is how).
- if you have a suggestion for improvement, write an issue here. (here is how)
- if you found a bug or problem with the app, write an issue here. (here is how)
- if you can improve the UI or Code, send a pull request. (here is how)
- if you want to support this project with money, I'll make a sponsor button later.
