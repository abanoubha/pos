module github.com/abanoubhannaazer/posapp

go 1.17

require (
	gioui.org v0.0.0-20211012125015-0048f7be1d0f
	golang.org/x/exp v0.0.0-20211012155715-ffe10e552389
)

require (
	gioui.org/cpu v0.0.0-20210817075930-8d6a761490d2 // indirect
	gioui.org/shader v1.0.4 // indirect
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	golang.org/x/text v0.3.6 // indirect
)
